# Frontend

This is our application front-end.

## Installing dependencies

1. Install [node & npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
2. Run `npm install`

## Running the local environment

1. Run `npm start`
