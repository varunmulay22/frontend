import "./styles.css";

import { Router } from "./Router";
import { AuthenticationProvider } from "./utils/authentication";

export default function App() {
  return (
    <AuthenticationProvider>
      <Router />
    </AuthenticationProvider>
  );
}
