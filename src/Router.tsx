import { db } from "./mocks/db";
import { TeamPage } from "./pages/TeamPage";
import { ProjectPage } from "./pages/ProjectPage";
import { LoginPage } from "./pages/LoginPage";
import { RegisterPage } from "./pages/RegisterPage";

import { useContext } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AuthContext } from "./utils/authentication";

export function Router() {
  let team = db.teams[Object.keys(db.teams)[0]];
  const { authenticated } = useContext(AuthContext);
  return (
    <BrowserRouter>
      <div
        className="App"
        style={{
          display: "grid",
          gridTemplateColumns:
            "var(--sidebar-left) calc(100% - var(--sidebar-left) - var(--sidebar-right)) var(--sidebar-right)",
          gridTemplateRows: "auto",
          gridTemplateAreas: `"header header header"
        "main main sidebar"
        "footer footer footer"`,
        }}
      >
        <Routes>
          {authenticated === true ? (
            <>
              <Route path="projects/:project" element={<ProjectPage />}></Route>
              <Route path="" element={<TeamPage teamId={team.id} />} />
            </>
          ) : (
            <>
              <Route path="" element={<LoginPage />}></Route>
              <Route path="register" element={<RegisterPage />}></Route>
            </>
          )}
          <Route path="register" element={<RegisterPage />}></Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
