import { pick } from "./pick";

function genName() {
  let a = [
    "Weekly",
    "Monthly",
    "Friday",
    "Regular",
    "Surprise",
    "Exciting",
    "Creative",
    "Challenge:",
    "Tuesday",
    "February",
    "Spring",
    "Summer",
    "Winter",
    "Autumn",
    "March",
    "April",
    "May",
  ];
  let b = ["Marketing", "Algebra", "Design", "Planning", "Cheese", "Mechanical", "Sprint", "Baking", ""];
  let c = ["Update", "- AMA", "Webinar", "Session", "Challenge", "Competition", "Hackathon", "Meditation", ""];
  return `${pick(a)} ${pick(b)} ${pick(c)}`;
}

export interface Project {
  id: string;
  name: string;
  team: string;
  assets: string[];
}

export function createMockProject(): Project {
  return {
    id: crypto.randomUUID(),
    name: genName(),
    assets: [],
    team: "",
  };
}
