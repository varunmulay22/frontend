import { createMockUser, User, createMockProject, createMockFile, createMockTeam, pick, Project, Team, File } from ".";

interface Index<T> {
  [key: string]: T;
}

let users: Index<User> = {};
let teams: Index<Team> = {};
let projects: Index<Project> = {};
let files: Index<File> = {};

interface DB {
  users: Index<User>;
  teams: Index<Team>;
  projects: Index<Project>;
  files: Index<File>;
}

export const db: DB = {
  users,
  teams,
  projects,
  files,
};

for (let i = 0; i < 10; i++) {
  const user = createMockUser();
  users[user.id] = user;
}

for (let i = 0; i < 2; i++) {
  const team = createMockTeam();
  team.owner = pick(Object.keys(users));
  team.members = Array.from(
    new Set([
      pick(Object.keys(users)),
      pick(Object.keys(users)),
      pick(Object.keys(users)),
      pick(Object.keys(users)),
      pick(Object.keys(users)),
      pick(Object.keys(users)),
    ])
  );
  teams[team.id] = team;

  for (let j = 0; j < 10; j++) {
    const project = createMockProject();
    project.team = team.id;
    db.projects[project.id] = project;

    const fileCount = 5 + Math.random() * 5;
    for (let k = 0; k < fileCount; k++) {
      const file = createMockFile();
      file.project = project.id;
      db.files[file.id] = file;
    }
  }
}

export function getTeamProjects(teamId: string) {
  return Object.keys(db.projects)
    .map((projectId) => db.projects[projectId])
    .filter((project) => project.team === teamId);
}

export function getProjectFiles(projectId: string): File[] {
  return Object.keys(db.files)
    .map((fileId) => db.files[fileId])
    .filter((file) => file.project === projectId);
}

export function addProject(teamId: string, userId: string) {}
