import { pick } from "./pick";

export interface File {
  id: string;
  project: string;
  name: string;
  filetype: string;
  status: string;
  phase: string;
}

export function createMockFile(): File {
  return {
    id: crypto.randomUUID(),
    project: "",
    name: pick(["Draft", "Stinger", "Notes", "Ideas"]),
    filetype: pick(["image", "video", "linked asset", "audio", "pdf", "text", "subtitle"]),
    status: pick(["Needs Review", "In Review", "In Progress", "Done"]),
    phase: pick(["Ideation", "Pre-Production", "Production", "Post-Production", "Retrospective"]),
  };
}
