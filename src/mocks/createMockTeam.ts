import { pick } from "./pick";

export interface Team {
  id: string;
  name: string;
  owner: string;
  members: string[];
}

export function createMockTeam(): Team {
  return {
    id: crypto.randomUUID(),
    name: pick(["The A Team", "Brilliance In Art", "Big Education"]),
    owner: "",
    members: [],
  };
}
