import React, { useCallback, useContext, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Button } from "../components/foundation/Button";
import { Input } from "../components/foundation/Input";
import { Label } from "../components/foundation/Label";
import { Header } from "../components/header";
import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import { AuthContext } from "../utils/authentication";

const LoginContainer = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  grid-column: 2;
  padding: var(--spacing-lg);
  border-radius: var(--radius-brand-xl);
  box-shadow: var(--shadow-20);
  width: 380px;
  margin: var(--spacing-xl) auto;
  border: 1px solid var(--color-brand-primary-90);
`;

export function LoginPage() {
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const authContext = useContext(AuthContext);

  const logIn = useCallback(
    async (event: React.FormEvent | React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();

      authContext.logIn(username, password);
    },
    [username, password]
  );

  return (
    <>
      <Header title={`Synura`}></Header>
      <LoginContainer>
        <H1 style={{ textAlign: "center" }}>Welcome Back!</H1>
        {authContext.error && (
          <div
            style={{
              background: "var(--color-danger-100)",
              color: "var(--color-nu-90)",
              border: "1px solid var(--color-danger-100)",
              padding: "var(--spacing-sm)",
              marginBottom: "var(--spacing-md)",
              borderRadius: "var(--radius-sm)",
            }}
          >
            <Text size={8}>{authContext.error}</Text>
          </div>
        )}
        <form style={{ display: "grid", marginBottom: "var(--spacing-sm)", gap: "var(--spacing-md)" }} onSubmit={logIn}>
          <Label>
            <Text>Username</Text>
            <Input
              value={username}
              onChange={(event) => setUsername(event.target.value)}
              type="text"
              placeholder="username"
            />
          </Label>
          <Label>
            <Text>Password</Text>
            <Input
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              type="password"
              placeholder="password"
            />
          </Label>
          <Button
            type="submit"
            style={{ background: "var(--gradient-info)", color: "var(--color-nu-100)" }}
            onClick={logIn}
          >
            Log In
          </Button>
        </form>
        <Text size={8}>
          Don't have an account yet? <Link to="/register">Sign up</Link>
        </Text>
      </LoginContainer>
    </>
  );
}
