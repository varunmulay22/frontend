import styled from "styled-components";

import { db, getProjectFiles } from "../mocks/db";

import { Button } from "../components/foundation/Button";
import { Table } from "../components/table";
import { Header } from "../components/header";
import { Sidebar } from "../components/sidebar";
import { Text } from "../components/typography/text";
import { useParams } from "react-router";

interface ProjectPageProps {}

export const ProjectPage: React.FC<ProjectPageProps> = ({}) => {
  const params = useParams();

  const projectId = params.project || "";
  const project = db.projects[projectId];
  const team = db.teams[project.team];
  const files = getProjectFiles(projectId);

  const columns = [{ heading: "File" }, { heading: "Status" }, { heading: "Contributors" }, { heading: "Modified" }];

  return (
    <>
      <Header title={`Synura: ${team.name} | ${project.name}`}></Header>
      <div
        style={{
          gridArea: "main",
        }}
      >
        <Table
          columns={columns}
          rows={files.map((file) => [
            <Text>{file.name}</Text>,
            <Text>{file.status}</Text>,
            <Text>(A)(B)</Text>,
            <Text>Due in 2 weeks</Text>,
          ])}
        />
      </div>
      <Sidebar>
        <Text>test</Text>
      </Sidebar>
    </>
  );
};
