import React, { useCallback, useContext, useState } from "react";
import { Link, NavigationType, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { Button } from "../components/foundation/Button";
import { Input } from "../components/foundation/Input";
import { Label } from "../components/foundation/Label";
import { Header } from "../components/header";
import { H1 } from "../components/typography/h1";
import { Text } from "../components/typography/text";
import { AuthContext } from "../utils/authentication";

const LoginContainer = styled.main`
  background: var(--color-brand-coolgray);
  color: var(--color-nu-90);
  grid-column: 2;
  padding: var(--spacing-lg);
  border-radius: var(--radius-brand-xl);
  box-shadow: var(--shadow-20);
  width: 380px;
  margin: var(--spacing-xl) auto;
  outline: 2px solid var(--color-brand-primary-90);
`;

export function RegisterPage() {
  const navigate = useNavigate();

  const [fullName, setFullName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [passwordConfirm, setPasswordConfirm] = useState<string>("");

  const [dirtyFullName, setDirtyFullName] = useState(false);
  const [dirtyEmail, setDirtyEmail] = useState(false);
  const [dirtyPass, setDirtyPass] = useState(false);

  const authContext = useContext(AuthContext);

  if (authContext.authenticated) {
    navigate("/");
  }

  const register = useCallback(
    async (event: React.FormEvent | React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();

      authContext.register(fullName, email, password, passwordConfirm);
    },
    [fullName, email, password, passwordConfirm, password]
  );

  const emailParts = email.split("@");
  const emailDomain = emailParts.length > 1 ? emailParts[1].split(".") : [];
  const validEmail =
    emailParts.length === 2 &&
    emailDomain.length > 1 &&
    emailDomain.reduce<boolean>((acc, curr) => curr.length > 0 && acc, true);
  const validPassword = password.length > 0 && password === passwordConfirm;
  const canSubmit = validPassword && fullName.length > 0 && email.length > 0;

  return (
    <>
      <Header title={`Synura`}></Header>
      <LoginContainer>
        <H1 style={{ textAlign: "center" }}>Welcome!</H1>
        {authContext.error && (
          <div
            style={{
              background: "var(--color-danger-100)",
              color: "var(--color-nu-90)",
              border: "1px solid var(--color-danger-100)",
              padding: "var(--spacing-sm)",
              marginBottom: "var(--spacing-md)",
              borderRadius: "var(--radius-sm)",
            }}
          >
            <Text size={8}>{authContext.error}</Text>
          </div>
        )}
        <form
          style={{ display: "grid", marginBottom: "var(--spacing-sm)", gap: "var(--spacing-md)" }}
          onSubmit={register}
        >
          <Label>
            <Text>Full Name</Text>
            <Input
              value={fullName}
              onChange={(event) => setFullName(event.target.value)}
              onBlur={() => setDirtyFullName(true)}
              type="text"
              placeholder="John Doe"
              required
            />
            {dirtyFullName && fullName.length === 0 && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Name required
              </Text>
            )}
          </Label>
          <Label>
            <Text>Email Address</Text>
            <Input
              value={email}
              onChange={(event) => setEmail(event.target.value)}
              onBlur={() => setDirtyEmail(true)}
              type="text"
              placeholder="john.doe@example.com"
            />
            {dirtyEmail && email.length === 0 && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Email address required
              </Text>
            )}
            {dirtyEmail && email.length > 0 && !validEmail && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Email address is invalid
              </Text>
            )}
          </Label>
          <Label>
            <Text>Password</Text>
            <Input
              value={password}
              onChange={(event) => setPassword(event.target.value)}
              type="password"
              placeholder="password"
            />
          </Label>
          <Label>
            <Text>Confirm Password</Text>
            <Input
              value={passwordConfirm}
              onChange={(event) => setPasswordConfirm(event.target.value)}
              type="password"
              placeholder="password"
              onBlur={(event) => setDirtyPass(true)}
            />
            {dirtyPass && !validPassword && (
              <Text size={8} style={{ color: "var(--color-danger-100)" }}>
                Passwords do not match
              </Text>
            )}
          </Label>
          <Button
            type="submit"
            disabled={!canSubmit}
            style={{ background: `var(--gradient-info)`, color: "var(--color-nu-90)" }}
            onClick={register}
          >
            Register
          </Button>
        </form>
        <Text size={8}>
          Already have an account? <Link to="/">Sign In</Link>
        </Text>
      </LoginContainer>
    </>
  );
}
