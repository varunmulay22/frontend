import { db, getTeamProjects } from "../mocks/db";

import { Button } from "../components/foundation/Button";
import { Text } from "../components/typography/text";
import { Grid } from "../components/layout/grid";
import { Header } from "../components/header";
import { ProjectCard } from "../components/project-card";
import { Sidebar } from "../components/sidebar";

interface TeamPageProps {
  teamId: string;
}

export const TeamPage: React.FC<TeamPageProps> = ({ teamId }) => {
  const team = db.teams[teamId];
  const projects = getTeamProjects(teamId);

  return (
    <>
      <Header title={`Synura: ${team.name}`} />
      <Grid
        gap="var(--spacing-md)"
        itemWidth="calc(var(--spacing-xxl) * 4)"
        style={{ padding: "var(--spacing-md) var(--spacing-xxl)" }}
      >
        {projects.map((project) => (
          <ProjectCard key={project.id} id={project.id} name={project.name} />
        ))}
      </Grid>
      <Sidebar>
        <Text>test</Text>
      </Sidebar>
    </>
  );
};
