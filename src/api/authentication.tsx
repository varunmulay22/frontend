import { AxiosError } from "axios";
import { request } from "./request";

export async function logIn(email: string, password: string) {
  try {
    const {
      status,
      data,
      headers: { authorization },
    } = await request.post("/users/sign_in", {
      user: {
        email,
        password,
      },
    });
    if (status === 201) {
      return { ...data, authorization };
    }
    throw new Error();
  } catch (error: AxiosError | unknown) {
    if ((error as AxiosError).name === "AxiosError") {
      return (error as AxiosError)?.response?.data;
    }
    throw new Error();
  }
}

export async function register(full_name: string, email: string, password: string, password_confirmation: string) {
  try {
    const {
      status,
      data,
      headers: { authorization },
    } = await request.post("/users", {
      user: {
        full_name,
        email,
        password,
        password_confirmation,
      },
    });
    if (status === 200) {
      return { ...data, authorization };
    }

    throw new Error();
  } catch (error: AxiosError | unknown) {
    if ((error as AxiosError).name === "AxiosError") {
      return (error as AxiosError)?.response?.data;
    }

    throw new Error();
  }
}
