import axios from "axios";

const baseConfig = {
  baseURL: process.env.REACT_APP_API_URL,
  origin: process.env.PUBLIC_URL,
};

export const request = axios.create(baseConfig);

export const authenticatedRequest = axios.create({
  ...baseConfig,
});

authenticatedRequest.interceptors.request.use((req) => {
  if (!req.headers) {
    req.headers = {};
  }
  const token = localStorage.getItem("synura-bearer-token");
  if (token) {
    req.headers.Authorization = token;
  }
});
