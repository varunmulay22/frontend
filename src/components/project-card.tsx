import { useCallback } from "react";
import { useNavigate } from "react-router";

import { Text } from "../components/typography/text";

interface ProjectCardProps {
  id: string;
  name: string;
}

export const ProjectCard: React.FC<ProjectCardProps> = ({ id, name }) => {
  const navigateTo = useNavigate();
  const onClick = useCallback(() => navigateTo(`/projects/${id}`), []);

  return (
    <div
      style={{
        height: "144px",
        color: "var(--color-nu-90)",
        background: "var(--color-brand-coolgray)",
        borderRadius: "var(--radius-brand-lg)",
        padding: "var(--spacing-md)",
        boxShadow: "0px 1px 2px -1px rgba(0,0,0,0.2),0px 1px 10px -5px rgba(0,0,0,0.5)",
        cursor: "pointer",
      }}
      onClick={onClick}
    >
      <Text
        style={{
          textAlign: "right",
        }}
      >
        {name}
      </Text>
    </div>
  );
};
