interface GridProps {
  children: React.ReactNode;
  style: React.CSSProperties;
  itemWidth: string;
  gap: string;
}

export const Grid: React.FC<GridProps> = ({ children, style, gap, itemWidth }) => (
  <div
    style={{
      paddingTop: "0px",
      display: "grid",
      gap,
      justifyContent: "space-between",
      gridTemplateColumns: `repeat(auto-fill, ${itemWidth})`,
      ...style,
    }}
  >
    {children}
  </div>
);
