import { H1 } from "./typography/h1";

interface SidebarProps {
  children?: React.ReactNode;
}

export const Sidebar: React.FC<SidebarProps> = ({ children }) => (
  <div
    style={{
      background: "var(--color-brand-coolgray)",
      padding: "var(--spacing-md)",
      color: "var(--color-nu-70)",
      marginLeft: "var(--spacing-md)",
      marginRight: "var(--spacing-md)",
      gridArea: "sidebar",
    }}
  >
    {children}
  </div>
);
