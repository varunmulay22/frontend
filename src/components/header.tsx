import { AuthenticationProvider, useAuthentication } from "../utils/authentication";
import { Button } from "./foundation/Button";
import Logo from "./logo";
import { LogoText } from "./typography/logotext";

interface HeaderProps {
  title: string;
  children?: React.ReactNode;
}

export const Header: React.FC<HeaderProps> = ({ title, children }) => {
  const authContext = useAuthentication();
  return (
    <div
      style={{
        gridArea: "header",
        display: "flex",
        gap: "var(--spacing-md)",
        background: "var(--color-nu-0)",
        padding: "var(--spacing-lg) var(--spacing-lg)",
        borderBottom: "1px solid var(--color-brand-coolgray)",
        marginBottom: "var(--spacing-md)",
      }}
    >
      <Logo height={17} />
      <LogoText
        style={{
          color: "var(--color-nu-100)",
        }}
      >
        {title}
      </LogoText>
      {children}
      {/*<Button label="create project" onClick={() => {}} />*/}
      <div style={{ marginLeft: "auto" }}>
        {authContext.authenticated && <Button onClick={authContext.logOut}>Log Out</Button>}
      </div>
    </div>
  );
};
