import { createStyleObject } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/montserrat";
import styled from "styled-components";

export const LogoText = styled.h1({
  fontFamily: "Montserrat",
  fontWeight: "500",
  letterSpacing: ".2rem",
  textTransform: "uppercase",
  alignItems: "center",
  justifyContent: "center",
  marginTop: "3px",
  ...createStyleObject({
    capHeight: 9,
    fontMetrics,
  }),
});
