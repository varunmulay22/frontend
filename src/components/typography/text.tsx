import { createStyleObject, createStyleString } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/montserrat";
import styled from "styled-components";

interface TextProps {
  size?: number;
  lineGap?: number;
  children: React.ReactNode;
}

export const Text = styled.span<TextProps>((props) => ({
  fontFamily: "Montserrat",
  ...createStyleObject({
    capHeight: props.size || 11,
    lineGap: props.lineGap || 4,
    fontMetrics,
  }),
}));
