import { createStyleObject } from "@capsizecss/core";
import fontMetrics from "@capsizecss/metrics/montserrat";
import styled from "styled-components";

export const H1 = styled.h1({
  fontFamily: "Montserrat",
  fontWeight: "normal",
  marginBottom: "var(--spacing-lg)",
  ...createStyleObject({
    capHeight: 20,
    lineGap: 16,
    fontMetrics,
  }),
});
