import styled from "styled-components";

export const Label = styled.label`
  display: flex;
  flex-direction: column;
  gap: var(--spacing-sm);
`;
