import styled from "styled-components";

export const Input = styled.input`
  height: var(--spacing-lg);
  padding-left: var(--spacing-sm);
  background: var(--color-nu-0);
  border: 1px solid var(--color-nu-10);
  border-radius: var(--radius-sm);
  color: var(--color-nu-90);
`;
