import React, { ButtonHTMLAttributes } from "react";

export const Button: React.FC<React.ButtonHTMLAttributes<HTMLButtonElement>> = ({ style, ...props }) => (
  <button
    style={{
      padding: "var(--spacing-sm) var(--spacing-md)",
      border: "none",
      background: "var(--color-nu-100)",
      boxShadow: "var(--shadow-10)",
      borderRadius: "var(--radius-sm)",
      cursor: "pointer",
      opacity: props.disabled ? 0.5 : 1.0,
      ...style,
    }}
    {...props}
  />
);
