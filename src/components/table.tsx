import styled from "styled-components";
import { Text } from "./typography/text";

const StyledTable = styled.table`
  display: grid;
  thead,
  tbody,
  tr {
    display: contents;
  }
  td {
    padding: var(--spacing-sm);
  }
  td:nth-child(1n) {
    background: var(--color-nu-70);
  }
  tr:nth-child(2n) td {
    background: var(--color-nu-90);
  }
  th {
    text-align: left;
    padding: var(--spacing-sm);
    background: var(--color-brand-coolgray);
    color: var(--color-nu-90);
    font-weight: normal;
  }
`;

export interface Column {
  size?: string;
  heading?: string;
}

export interface TableProps {
  columns: Column[];
  rows: React.ReactNode[][];
}

export const Table: React.FC<TableProps> = ({ columns, rows }) => {
  return (
    <StyledTable
      style={{
        gridTemplateColumns: columns.map(({ size }) => size || "auto").join(" "),
      }}
    >
      <thead>
        <tr>
          {columns.map((cell) => (
            <th>
              <Text>{cell.heading}</Text>
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {rows.map((row) => (
          <tr>
            {row.map((cell) => (
              <td>{cell}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </StyledTable>
  );
};
