import { useNavigate } from "react-router-dom";

export default function logo({ width, height }: { width?: number; height?: number }) {
  let navigate = useNavigate();
  const routeChange = () => {
    navigate("/");
  };

  return (
    <svg
      onClick={routeChange}
      id="Layer_1"
      data-name="Layer 1"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 753.32 716.21"
      width={width}
      height={height}
    >
      <defs>
        <style>{`.cls-1 { fill: #ff4f2e; }`} </style>
      </defs>
      <path
        className="cls-1"
        d="M318.43,0A173.36,173.36,0,0,0,168.29,86.68L0,378.17H240.25l98.22-170.1H633.19L753.32,0Z"
      />
      <path
        className="cls-1"
        d="M434.89,716.21A173.33,173.33,0,0,0,585,629.53L753.32,338H513.06l-98.21,170.1H120.13L0,716.21Z"
      />
    </svg>
  );
}
