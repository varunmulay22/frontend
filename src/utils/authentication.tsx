import React, { createContext, useCallback, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { logIn, register } from "../api/authentication";
import { authenticatedRequest } from "../api/request";

interface AuthContextProps {
  authenticated: boolean;
  logIn: (username: string, password: string) => void;
  register: (fullName: string, email: string, password: string, passwordConfirm: string) => void;
  logOut: () => void;
  user?: { name: string; id: string };
  error?: string;
}

export const AuthContext = createContext<AuthContextProps>({
  authenticated: false,
  logIn: (username, password) => logIn(username, password),
  register: (fullName, email, password, passwordConfirm) => {},
  logOut: () => {},
});

export const AuthenticationProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  const [error, setError] = useState<string | undefined>(undefined);
  const [authenticated, setAuthenticated] = useState<boolean>(false);
  const [user, setUser] = useState<any>();

  const logInCallback = useCallback(async (username: string, password: string) => {
    const response = await logIn(username, password);

    if (response.error) {
      setError(response.error);
      setAuthenticated(false);
    } else {
      let { authorization, ...user } = response;
      setAuthenticated(true);
      setUser(user);
      localStorage.setItem("synura-bearer-token", authorization);
      authenticatedRequest.defaults.headers.common.Authorization = authorization;
    }
  }, []);

  const registerCallback = useCallback(
    async (fullName: string, email: string, password: string, passwordConfirm: string) => {
      const response = await register(fullName, email, password, passwordConfirm);

      if (response.error) {
        setError(response.error);
        setAuthenticated(false);
      } else {
        let { authorization, ...user } = response;
        setAuthenticated(true);
        setUser(user);

        localStorage.setItem("synura-bearer-token", authorization);
        authenticatedRequest.defaults.headers.common.Authorization = authorization;
      }
    },
    []
  );

  const logOutCallback = useCallback(() => {
    setAuthenticated(false);
    localStorage.removeItem("synura-bearer-token");
  }, []);

  useEffect(() => {
    const token = localStorage.getItem("synura-bearer-token");
    if (!token) {
      setAuthenticated(false);
    } else {
      const parts = token.split(" ");
      const jwt = parts[1].split(".");
      const user = JSON.parse(window.atob(jwt[1]));
      if (user.exp * 1000 > Date.now()) {
        setAuthenticated(true);
      }
    }
  }, []);

  return (
    <AuthContext.Provider
      value={{ authenticated, error, logIn: logInCallback, register: registerCallback, logOut: logOutCallback }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuthentication() {
  return useContext(AuthContext);
}
